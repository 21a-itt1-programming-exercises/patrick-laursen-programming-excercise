#Exercise 1: Type the following statements in the Python interpreter to
#see what they do:

5
x = 5
x + 1


print(x+1)

#Exercise 2:

name = input("Enter your name: ")
print("Hello,",name)


#Exercise 3:

hours = float(input("Enter hours: "))
rate = float(input("Enter rate: "))
print("Pay:",hours*rate)

#Exercise 4: Assume that we execute the following assignment state-
#ments:
#width = 17
#height = 12.0
#For each of the following expressions, write the value of the expression and the
#type (of the value of the expression).

width = 17
height = 12.0

print(width//2, type(width//2))
print(width/2, type(width/2))
print(height/3, type(height/3))
print(1+2*5,type(1+2*5))

#Exercise 5
celcius_temp = float(input("Please input tempeareture in Celcius:"))
fahrenheit_temp = (9.0/5.0)*celcius_temp + 32.0
print(celcius_temp,"degrees celcis is equal to",fahrenheit_temp,"degrees fahrenheit")